﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.CrossPlatformInput;

namespace CompleteProject
{
    public class PlayerMovement : MonoBehaviour
    {
        public float speed = 9f;            // The speed that the player will move at.
        private float jumpForce = 600;
        public int maxNumberOfJumps = 6;
        AudioSource playerAudioSource;
        public AudioClip playerJump;
        public GameObject[] powerUpSpawns;
        public GameObject powerUpPrefab;

        int timesJumped = 0;

        Vector3 movement;                   // The vector to store the direction of the player's movement.
        Animator anim;                      // Reference to the animator component.
        Rigidbody playerRigidbody;          // Reference to the player's rigidbody.
#if !MOBILE_INPUT
        int floorMask;                      // A layer mask so that a ray can be cast just at gameobjects on the floor layer.
        float camRayLength = 100f;          // The length of the ray from the camera into the scene.
#endif

        private int powerUpIndex;

        void Awake()
        {
#if !MOBILE_INPUT
            // Create a layer mask for the floor layer.
            floorMask = LayerMask.GetMask("Floor");
#endif

            // Set up references.
            anim = GetComponent<Animator>();
            playerRigidbody = GetComponent<Rigidbody>();

            playerAudioSource = GetComponentInChildren<AudioSource>();
        }

        private void Update()
        {
            // store the jump input.
            bool jump = Input.GetButtonDown("Jump");
            // Jump Physics
            if (Mathf.Round(playerRigidbody.velocity.y * 10) == 0)
            {
                timesJumped = 0;
            }
            // Dont allow the player to jump more than the set number of times in the inspector
            if (jump && timesJumped < maxNumberOfJumps)
            {
                playerRigidbody.velocity = new Vector3(playerRigidbody.velocity.x, 0, playerRigidbody.velocity.z);
                playerRigidbody.AddForce(Vector3.up * jumpForce);
                timesJumped++;
                playerAudioSource.PlayOneShot(playerJump);
            }
        }
        void FixedUpdate()
        {

            // Store the input axes.
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");

            // Move the player around the scene.
            Move(h, v);

            // Turn the player to face the mouse cursor.
            Turning();

            // Animate the player.
            Animating(h, v);
        }


        void Move(float h, float v)
        {
            // Set the movement vector based on the axis input.
            movement.Set(h, 0f, v);

            // Normalise the movement vector and make it proportional to the speed per second.
            movement = movement.normalized * speed * Time.deltaTime;

            // Move the player to it's current position plus the movement.
            playerRigidbody.MovePosition(transform.position + movement);
        }


        void Turning()
        {
#if !MOBILE_INPUT
            // Create a ray from the mouse cursor on screen in the direction of the camera.
            Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            // Create a RaycastHit variable to store information about what was hit by the ray.
            RaycastHit floorHit;

            // Perform the raycast and if it hits something on the floor layer...
            if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
            {
                // Create a vector from the player to the point on the floor the raycast from the mouse hit.
                Vector3 playerToMouse = floorHit.point - transform.position;

                // Ensure the vector is entirely along the floor plane.
                playerToMouse.y = 0f;

                // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
                Quaternion newRotatation = Quaternion.LookRotation(playerToMouse);

                // Set the player's rotation to this new rotation.
                playerRigidbody.MoveRotation(newRotatation);
            }
#else

            Vector3 turnDir = new Vector3(CrossPlatformInputManager.GetAxisRaw("Mouse X") , 0f , CrossPlatformInputManager.GetAxisRaw("Mouse Y"));

            if (turnDir != Vector3.zero)
            {
                // Create a vector from the player to the point on the floor the raycast from the mouse hit.
                Vector3 playerToMouse = (transform.position + turnDir) - transform.position;

                // Ensure the vector is entirely along the floor plane.
                playerToMouse.y = 0f;

                // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
                Quaternion newRotatation = Quaternion.LookRotation(playerToMouse);

                // Set the player's rotation to this new rotation.
                playerRigidbody.MoveRotation(newRotatation);
            }
#endif
        }


        void Animating(float h, float v)
        {
            // Create a boolean that is true if either of the input axes is non-zero.
            bool walking = h != 0f || v != 0f;

            // Tell the animator whether or not the player is walking.
            anim.SetBool("IsWalking", walking);
        }

        //Activate power up is player runs into object tagged as power up
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "PowerUp")
            {
                Debug.Log("is Powered Up");

                PowerUpActivate();

                Destroy(other.gameObject);
            }
        }


        void PowerUpActivate()
        {
            powerUpIndex = Random.Range(0, 5);

            if (powerUpIndex <= 3)
            {
                Debug.Log("Speed Up");
                speed = speed + 1;

            }
            else if (powerUpIndex >= 4)
            {
                Debug.Log("Speed Down");
                speed = speed - 1;
            }

            Instantiate(powerUpPrefab, powerUpSpawns[powerUpIndex].transform.position, Quaternion.identity);
        }
    }
}