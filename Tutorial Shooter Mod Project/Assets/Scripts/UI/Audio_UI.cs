﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio_UI : MonoBehaviour {

    public AudioClip resumeAudio, quitAudio;
    AudioSource myAudioSource;

    private void Start()
    {
        myAudioSource = GetComponent<AudioSource>();
    }

    public void PlayResumeButtonAudio()
    {
        myAudioSource.PlayOneShot(resumeAudio);
    }

    public void PlayQuitButtonAudio()
    {
        myAudioSource.PlayOneShot(quitAudio);
    }
}
