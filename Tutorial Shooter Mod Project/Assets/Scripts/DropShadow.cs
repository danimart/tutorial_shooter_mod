﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropShadow : MonoBehaviour {

    public GameObject player;

	// Use this for initialization
	void Start () {
        transform.parent = null;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);


        float newScale = 1 - (player.transform.position.y * .2f);
        transform.localScale = new Vector3(newScale, newScale, newScale);

	}
}
